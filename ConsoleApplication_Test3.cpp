﻿#include <iostream>
using namespace std;

void Odd_Even(int limit, bool isOdd) 
{
   for (int i = 0; i <= limit; ++i)
   {
     if (i % 2 == isOdd)
     cout << i << " ";
   }
} //проверка четности и вывод чисел от 0 до лимита

int main()
{
    setlocale(0, "Russian");
    cout << "Программа выводит все четные числа от 0 до числа, которое вы укажите.\n";
    cout << "Введите число: ";
    int a;
    cin >> a;
    cout << "Введите '0' для отображения чётных, либо любое другое число для отображения нечётных: ";
    int b;
    cin >> b;

    Odd_Even (a, b);

    return 0;                 
}